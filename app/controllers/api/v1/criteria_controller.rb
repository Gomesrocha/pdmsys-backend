# frozen_string_literal: true
module Api
  module V1
    class CriteriaController < ApiController
      before_action :set_criterium, only: [:show, :update, :destroy]

      ACCESS_LEVEL = 1

      # GET /criteria
      def index
        offset = params[:offset] ? params[:offset].to_i : 0
        limit = params[:limit] ? params[:limit].to_i : 1000

        if params[:name]
          @criteria = Criterium.by_availability.limit(limit).offset(offset)
                               .by_name(params[:name])
          total = Criterium.by_availability.by_name(params[:name]).count
        else
          @criteria = Criterium.by_availability.limit(limit).offset(offset)
          total = Criterium.by_availability.count
        end

        query_params = {
          counter: @criteria.length,
          limit: limit,
          offset: offset,
          total: total
        }

        success_response(@criteria, :ok, query_params)
      end

      # GET /criteria/1
      def show
        success_response(@criterium)
      end

      # POST /criteria
      def create
        @criterium = Criterium.create!(criterium_params)
        success_response({ criterium: { id: @criterium.id } }, :created)
      end

      # PATCH/PUT /criteria/1
      def update
        @criterium.update!(criterium_params)
        success_response({ criterium: { id: @criterium.id } })
      end

      # DELETE /criteria/1
      def destroy
        @criterium.destroy!
        add_to_timeline(@criterium.name)
        head :no_content
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_criterium
        @criterium = Criterium.find(params[:id])
        unless CurrentScope.user['access_level'] >= ACCESS_LEVEL ||
               CurrentScope.user['id'] == @criterium.user_id
          not_found_response(['Registro não encontrado'])
        end
      end

      # Only allow a trusted parameter "white list" through.
      def criterium_params
        params.require(:criterium).permit(
          :name, :description, :kind, :default_item, :user_id
        )
      end
    end
  end
end
