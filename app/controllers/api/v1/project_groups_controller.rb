# frozen_string_literal: true
module Api
  module V1
    class ProjectGroupsController < ApiController
      before_action :set_project_group, only: [:update, :destroy]
      serialization_scope :action_name

      require 'matrix'
      require 'wannabe_bool'

      ACCESS_LEVEL = 1
      ASSOCIATIONS = %i(
        criteria
        criteria_project_groups
        criteria_projects
        projects
        project_dependents
      ).freeze

      # GET /project_groups
      def index
        offset = params[:offset] ? params[:offset].to_i : 0
        limit = params[:limit] ? params[:limit].to_i : 1000

        if params[:name]
          @project_groups = ProjectGroup.limit(limit).offset(offset)
                                        .by_name(params[:name])
                                        .by_current_user.order(id: :desc)
          total = ProjectGroup.by_name(params[:name])
                              .by_current_user.count
        else
          @project_groups = ProjectGroup.limit(limit).offset(offset)
                                        .by_current_user.order(id: :desc)
          total = ProjectGroup.by_current_user.count
        end

        query_params = {
          counter: @project_groups.length,
          limit: limit,
          offset: offset,
          total: total
        }

        success_response(@project_groups, :ok, query_params)
      end

      # GET /project_groups/1
      def show # rubocop:disable Metrics/MethodLength
        @project_group = ProjectGroup.includes(ASSOCIATIONS).find(params[:id])
        check_access(@project_group.user_id)
        criteria_projects = @project_group.criteria_projects.to_a
        criteria_project_groups = @project_group.criteria_project_groups.to_a
        projects = @project_group.projects.to_a
        project_dependents = @project_group.project_dependents.to_a

        columns = []
        distances = {}
        distances[:positives] = []
        distances[:negatives] = []
        matrix = Matrix[]
        relative_distances = []
        solutions = {}
        solutions[:positives] = []
        solutions[:negatives] = []
        weight_sum = criteria_project_groups.sum(&:weight)

        # Calculation of percentages and normalization of matrix

        criteria_project_groups.each do |criteria_project_group|
          curr_weight = criteria_project_group.weight
          curr_percent = (curr_weight * 100.0 / weight_sum).round(2)

          next if curr_percent.nan? || curr_percent.zero?

          attrs = []
          squared = 0
          temp_array = []
          values = criteria_projects.select do |value|
            value.criterium_id == criteria_project_group.criterium_id
          end

          values.each do |value|
            val = value.attr.to_f
            squared += val**2
            attrs << val
          end

          square_root = Math.sqrt(squared)

          attrs.each do |attr|
            temp_array << curr_percent * attr / square_root
          end

          columns << temp_array

          matrix = Matrix.rows(columns)

          min, max = temp_array.minmax
          solutions[:positives] << max
          solutions[:negatives] << min
        end

        # Calculation of distances (positives / negatives)

        column_vectors = matrix.column_vectors

        column_vectors.each do |values|
          positive_sum = 0
          negative_sum = 0
          values.each.with_index do |value, index|
            positive_sum += (solutions[:positives].at(index) - value)**2
            negative_sum += (solutions[:negatives].at(index) - value)**2
          end
          distances[:positives] << Math.sqrt(positive_sum)
          distances[:negatives] << Math.sqrt(negative_sum)
        end

        # Calculation of relative distances

        projects.each.with_index do |project, index|
          curr_positive = distances[:positives].at(index)
          curr_negative = distances[:negatives].at(index)

          begin
            curr_distance = curr_negative / (curr_positive + curr_negative)
          rescue
            curr_distance = 0.0
          ensure
            curr_distance = 0.0 if curr_distance.nil? || curr_distance.nan?
          end

          project_dependents_ids = []

          project_dependents.each do |value|
            next unless value.project_id == project.id

            project_dependents_ids << value.project_dependent_id
          end

          relative_distances << {
            distance: curr_distance,
            project_id: project.id,
            project_name: project.name,
            project_dependents_ids: project_dependents_ids
          }
        end

        # Sort of relative distances

        relative_distances = custom_sort(relative_distances)

        query_params = {
          relative_distances: relative_distances
        }

        success_response(@project_group, :ok, query_params, ASSOCIATIONS)
      end

      def edit
        @project_group = ProjectGroup.includes(ASSOCIATIONS).find(params[:id])
        check_access(@project_group.user_id)
        success_response(@project_group, :ok, {}, ASSOCIATIONS)
      end

      # POST /project_groups
      def create
        ActiveRecord::Base.transaction do
          @project_group = ProjectGroup.create!(project_group_params)
          success_response({ project_group: { id: @project_group.id } },
                           :created)
        end
      end

      # PATCH/PUT /project_groups/1
      def update
        projects_attributes = params[:project_group][:projects_attributes]
        if circular_dependency?(@project_group, projects_attributes)
          unprocessable_response(@project_group.errors.full_messages)
        else
          ActiveRecord::Base.transaction do
            @project_group.update!(project_group_params)
            handle_dependents(projects_attributes)
            success_response(@project_group, :created)
          end
        end
      end

      # DELETE /project_groups/1
      def destroy
        @project_group.destroy!
        add_to_timeline(@project_group.name)
        head :no_content
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_project_group
        @project_group = ProjectGroup.find(params[:id])
        check_access(@project_group.user_id)
      end

      # Only allow a trusted parameter "white list" through.
      def project_group_params
        params.require(:project_group).permit(
          :name, :description, :user_id,
          projects_attributes: [
            :id, :name, :description, :_destroy,
            criteria_projects_attributes: [
              :id, :attr, :criterium_id, :project_id, :_destroy
            ],
            project_dependents_attributes: [
              :project_dependent_id, :_destroy, :_new
            ]
          ],
          criteria_project_groups_attributes: [
            :id, :weight, :criterium_id, :project_group_id
          ]
        )
      end

      def check_access(user_id)
        unless CurrentScope.user['access_level'] >= ACCESS_LEVEL ||
               CurrentScope.user['id'] == user_id
          not_found_response(['Registro não encontrado'])
        end
      end

      def circular_dependency?(project_group, projects_attributes)
        temp_hash = projects_attributes.each.with_object({}) do |o, h|
          next if o[:_destroy].to_b ||
                  (!o[:id] && !o[:project_dependents_attributes])

          arr = []
          o[:project_dependents_attributes].each do |value|
            arr << value[:project_dependent_id] unless value[:_destroy].to_b
          end
          h[o[:id]] = arr
        end

        Sorter.new(temp_hash).tsort && false

      rescue TSort::Cyclic
        project_group.errors.add(:base,
                                 'Dependência circular detectada') && true
      rescue StandardError
        project_group.errors.add(:base,
                                 'Falha ao salvar o grupo de projetos. '\
                                 'Por favor, tente novamente') && true
      end

      def custom_sort(arr)
        dup_arr = arr.dup

        projects_hash = dup_arr.each.with_object({}) do |o, h|
          h[o[:project_id]] = o
        end

        dup_arr.sort_by! { |a| a[:distance] }.reverse!

        temp_hash = dup_arr.each.with_object({}) do |o, h|
          next unless o[:project_id] && o[:project_dependents_ids]

          h[o[:project_id]] = o[:project_dependents_ids]
        end

        sorted = Sorter.new(temp_hash).tsort
        projects_hash.values_at(*sorted)
      end

      # rubocop:disable Metrics/MethodLength
      def handle_dependents(projects_attributes)
        delete_query = ''
        new_dependents = []

        projects_attributes.each do |project|
          next if project[:_destroy].to_b

          curr_dependents = project[:project_dependents_attributes]
          curr_project_id = project[:id]
          old_dependents = []

          curr_dependents.each do |dependent|
            dependent_id = dependent[:project_dependent_id]

            old_dependents << dependent_id if dependent[:_destroy].to_b

            if dependent[:_new].to_b
              new_dependents << "(#{curr_project_id}, #{dependent_id})"
            end
          end

          next if old_dependents.empty?

          delete_query += 'OR ' unless delete_query.blank?
          delete_query += %{
            `project_dependents`.`project_id` = #{curr_project_id} AND
            (`project_dependents`.`project_dependent_id`
            IN (#{old_dependents.join(', ')}))
          }.squish + ' '
        end

        unless delete_query.blank?
          ActiveRecord::Base.connection.execute(
            "DELETE FROM `project_dependents` WHERE #{delete_query}"
          )
        end

        return if new_dependents.empty?

        ActiveRecord::Base.connection.execute(
          %{
            INSERT INTO `project_dependents`
            (`project_id`, `project_dependent_id`)
            VALUES #{new_dependents.join(', ')}
          }.squish
        )
      end
    end
  end
end
