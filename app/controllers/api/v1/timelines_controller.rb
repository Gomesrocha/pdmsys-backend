# frozen_string_literal: true
module Api
  module V1
    class TimelinesController < ApplicationController
      ACCESS_LEVEL = 4
      ASSOCIATIONS = %i(
        timeline_category
        user
      ).freeze

      # GET /timelines
      def index
        if CurrentScope.user['access_level'] < ACCESS_LEVEL
          not_found_response(['Sem permissão'])
        end

        offset = params[:offset] ? params[:offset].to_i : 0
        limit = params[:limit] ? params[:limit].to_i : 1000
        begin
          date = DateTime.parse(params[:date])
        rescue ArgumentError
          date = DateTime.now
        end

        @timelines = Timeline.includes(ASSOCIATIONS).limit(limit).offset(offset)
                             .by_date(date).order(id: :desc)
        total = Timeline.by_date(date).count
        query_params = {
          counter: @timelines.length,
          limit: limit,
          offset: offset,
          total: total
        }

        success_response(@timelines, :ok, query_params, ASSOCIATIONS)
      end
    end
  end
end
