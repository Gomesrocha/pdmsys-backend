# frozen_string_literal: true
module Api
  module V1
    class UserTypesController < ApiController
      before_action :check_access_level
      before_action :set_user_type, only: [:show, :update, :destroy]

      ACCESS_LEVEL = 4

      # GET /user_types
      def index
        offset = params[:offset] ? params[:offset].to_i : 0
        limit = params[:limit] ? params[:limit].to_i : 1000

        if params[:name]
          @user_types = UserType.limit(limit).offset(offset)
                                .by_name(params[:name])
          total = UserType.by_name(params[:name]).count
        else
          @user_types = UserType.limit(limit).offset(offset)
          total = UserType.count
        end

        query_params = {
          counter: @user_types.length,
          limit: limit,
          offset: offset,
          total: total
        }

        success_response(@user_types, :ok, query_params)
      end

      # GET /user_types/1
      def show
        success_response(@user_type)
      end

      # POST /user_types
      def create
        @user_type = UserType.create!(user_type_params)
        success_response({ user_type: { id: @user_type.id } }, :created)
      end

      # PATCH/PUT /user_types/1
      def update
        @user_type.update!(user_type_params)
        success_response({ user_type: { id: @user_type.id } })
      end

      # DELETE /user_types/1
      def destroy
        if @user_type.access_level < 5
          @user_type.destroy!
          add_to_timeline(@user_type.name)
          head :no_content
        else
          not_found_response(['Sem permissão'])
        end
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_user_type
        @user_type = UserType.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def user_type_params
        params.require(:user_type).permit(:access_level, :name, :description)
      end

      def check_access_level
        return if CurrentScope.user['access_level'] >= ACCESS_LEVEL
        not_found_response(['Sem permissão'])
      end
    end
  end
end
