# frozen_string_literal: true
module Api
  module V1
    class UsersController < ApplicationController
      before_action :set_user, only: [:show, :update, :destroy]
      skip_before_action :authenticate, except:
        [:index, :show, :update, :destroy]

      ACCESS_LEVEL = 4

      # GET /users
      def index # rubocop:disable Metrics/MethodLength
        offset = params[:offset] ? params[:offset].to_i : 0
        limit = params[:limit] ? params[:limit].to_i : 1000

        if params[:name]
          @users = User.includes(:user_type).limit(limit).offset(offset)
                       .by_email(params[:name])
          total = User.by_email(params[:name]).count
        else
          @users = User.includes(:user_type).limit(limit).offset(offset)
          total = User.count
        end

        query_params = {
          counter: @users.length,
          limit: limit,
          offset: offset,
          total: total
        }

        if required_access_level?
          success_response(@users, :ok, query_params)
        else
          not_found_response(['Registro não encontrado'])
        end
      end

      # GET /users/1
      def show
        if required_access_level? || same_user?(@user.id)
          success_response(@user)
        else
          not_found_response(['Registro não encontrado'])
        end
      end

      # POST /users
      def create
        @user = User.create!(user_params)
        send_email_confirmation(@user)
        success_response({ user: { id: @user.id } }, :created)
      end

      # PATCH/PUT /users/1
      def update
        if required_access_level? || same_user?(@user.id)
          if @user.authenticate(params[:user][:current_password])
            @user.update!(user_params)
            success_response({ user: { id: @user.id } })
          else
            unprocessable_response(['Senha atual inválida'])
          end
        else
          not_found_response(['Registro não encontrado'])
        end
      end

      # DELETE /users/1
      def destroy
        if required_access_level? && !same_user?(@user.id)
          @user.destroy!
          add_to_timeline(@user.name || @user.email)
          head :no_content
        else
          not_found_response(['Registro não encontrado'])
        end
      end

      def confirm_email
        token = user_params[:token]
        decoded_token = Auth.decode(token)
        user = User.find_by(id: decoded_token['user'])

        if user.email_confirmed
          unprocessable_response(['E-mail já confirmado'])
        elsif user.email_confirmation_token == token
          user.update_columns(
            email_confirmed: true,
            email_confirmation_token: nil
          )
          add_to_timeline(user.name || user.email, user.id)
          send_header(user)
          head :no_content
        else
          unauthorized_response(['Link expirado/inválido'])
        end
      rescue StandardError
        unauthorized_response(['Link expirado/inválido'])
      end

      def recover_password
        user = User.find_by(email: user_params[:email].downcase)
        payload = Auth.generate_payload(user.id, 30.minutes.from_now.to_i)
        encoded_token = Auth.encode(payload)
        user.update_columns(reset_password_token: encoded_token)
        host = host_url
        link = "#{host}reset_password?token=#{user.reset_password_token}"
        browser = browser_instance
        UserMailer.password_recovery(
          link, user, '30 minutos', browser
        ).deliver_now!
        head :no_content
      rescue StandardError
        unprocessable_response(['E-mail inválido'])
      end

      def resend_email
        user = User.find_by(email: user_params[:email].downcase)
        send_email_confirmation(user)
        head :no_content
      rescue StandardError
        unprocessable_response(
          ['Falha ao reenviar e-mail. Contate o administrador do sistema.']
        )
      end

      def reset_password
        prs = user_params
        token = prs[:token]
        user = User.find_by(reset_password_token: token)

        user.update_columns(
          password_digest: BCrypt::Password.create(prs[:password]),
          reset_password_token: nil
        )
        add_to_timeline(user.name || user.email, user.id)
        head :no_content
      rescue StandardError
        unauthorized_response(['Link expirado/inválido'])
      end

      def sign_in
        prs = user_params
        user = User.find_by(email: prs[:email].downcase)
        if user.try(:authenticate, prs[:password])
          if user.email_confirmed
            send_header(user)
            add_to_timeline(user.name || user.email, user.id)
            head :no_content
          else
            unauthorized_response(['E-mail não confirmado'])
          end
        else
          unprocessable_response(['E-mail e/ou senha inválido(s)'])
        end
      end

      def sign_out
        # prs = user_params
        # add_to_timeline(user_params[:name] || user_params[:email])
        if CurrentScope.user
          CurrentScope.user = nil
        end
        head :no_content
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_user
        @user = User.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def user_params
        params.require(:user).permit(
          :encoded_avatar, :email, :name, :nickname, :password,
          :password_confirmation, :token
        )
      end

      def required_access_level?
        CurrentScope.user['access_level'] >= ACCESS_LEVEL
      end

      def same_user?(user_id)
        CurrentScope.user['id'] == user_id
      end

      def send_email_confirmation(user)
        payload = Auth.generate_payload(user.id, 24.hours.from_now.to_i)
        encoded_token = Auth.encode(payload)
        user.update_columns(email_confirmation_token: encoded_token)
        host = host_url
        link = "#{host}confirm_email?token=#{user.email_confirmation_token}"
        UserMailer.registration_confirmation(
          link, user, '24 horas'
        ).deliver_now!
      rescue StandardError
        false
      end

      def send_header(user)
        user_attributes = {
          access_level: user.user_type.access_level,
          avatar_url: user.avatar.instance.avatar_file_name && user.avatar_url,
          email: user.email,
          id: user.id,
          name: user.name
        }
        payload = Auth.generate_payload(user_attributes)
        handle_auth_header(Auth.encode(payload))
      end
    end
  end
end
