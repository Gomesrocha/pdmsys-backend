# frozen_string_literal: true
require 'auth'
require 'browser'

module CurrentScope
  thread_mattr_accessor :admin_user_types
  thread_mattr_accessor :user
end

class ApplicationController < ActionController::API
  after_action :add_to_timeline, only: [:create, :update]
  before_action :authenticate
  include ExceptionHandler
  include ResponseHandler
  include TimelineHandler

  private

  def authenticate
    token = request.headers['Authorization'].split(' ').last
    decoded_token = Auth.decode(token)
    token_payload = Auth.generate_payload(decoded_token['user'])
    encoded_token = Auth.encode(token_payload)
    handle_auth_header(encoded_token)
    CurrentScope.user = decoded_token['user']
    unless CurrentScope.admin_user_types
      CurrentScope.admin_user_types = UserType.where('access_level > 3')
                                              .pluck(:access_level)
    end
  rescue StandardError
    unauthorized_response(['Sessão expirada'])
  end

  def browser_instance
    Browser.new(request.env['HTTP_USER_AGENT'])
  end

  def handle_auth_header(token)
    response.headers['auth_token'] = token
  end

  def host_url
    request.referer[0..request.referer.rindex('/')]
  end
end
