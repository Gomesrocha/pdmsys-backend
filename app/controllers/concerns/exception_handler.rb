# frozen_string_literal: true
module ExceptionHandler
  extend ActiveSupport::Concern

  included do
    rescue_from ActiveRecord::RecordInvalid,
                ActiveRecord::RecordNotDestroyed do |e|
      e.record.errors.delete(:avatar)
      puts "e #{e}"
      unprocessable_response(e.record.errors.full_messages)
    end

    rescue_from ActiveRecord::RecordNotFound do
      not_found_response(['Registro não encontrado'])
    end
  end
end
