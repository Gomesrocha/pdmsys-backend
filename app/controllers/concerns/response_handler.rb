# frozen_string_literal: true
module ResponseHandler
  def success_response(obj, status = :ok, query_params = {},
                       associations = {})
    render_response(obj, status, query_params, associations)
  end

  def not_found_response(errors)
    render_response(errors, :not_found)
  end

  def unauthorized_response(errors)
    render_response(errors, :unauthorized)
  end

  def unprocessable_response(errors)
    render_response(errors, :unprocessable_entity)
  end

  private

  def render_response(data, status, query_params = {},
                      associations = {})
    int_status = Rack::Utils::SYMBOL_TO_STATUS_CODE[status]

    case int_status
    when 200..299
      render json: data, status: status, meta: query_params,
        include: associations
    else
      render json: { errors: data }, status: status
    end
  end
end
