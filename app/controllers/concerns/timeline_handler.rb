# frozen_string_literal: true
module TimelineHandler
  ACTIONS = {
    'create' => 'cadastrado(a)',
    'update' => 'alterado(a)',
    'destroy' => 'removido(a)',
    'confirm_email' => 'login',
    'reset_password' => 'troca de senha',
    'sign_in' => 'login',
    'sign_out' => 'logoff'
  }.freeze

  def add_to_timeline(name = nil, id = nil)
    browser = browser_instance
    current_action = ACTIONS[action_name]
    is_auth_action = %w(create update destroy).exclude?(action_name)

    if is_auth_action
      current_model = action_name.classify
      action = "efetuou #{current_action}"
      message = nil
    else
      current_controller = controller_name.singularize
      current_model = I18n.t("activerecord.models.#{current_controller}",
                             default: 'Não encontrado')
      action = "#{I18n.t(action_name)} #{current_model.downcase}"
      current_params = params[current_controller]
      current_name = name || current_params[:name] || current_params[:email]
      message = "#{current_model} <strong>#{current_name}</strong> #{current_action}."
    end

    timeline_category = TimelineCategory.find_by(name: current_model)

    Timeline.create(
      action: action,
      browser_name: browser.name,
      browser_version: browser.version,
      ip: request.remote_ip,
      message: message,
      timeline_category_id: timeline_category ? timeline_category.id : 1,
      user_id: id || CurrentScope.user['id']
    )
  end
end
