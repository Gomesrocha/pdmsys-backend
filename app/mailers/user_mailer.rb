# frozen_string_literal: true
class UserMailer < ApplicationMailer
  def registration_confirmation(link, user, human_time)
    @confirmation_link = link
    @human_time = human_time
    mail(
      from: Rails.configuration.email_settings['email'],
      to: user.email,
      subject: '[PDMSys] Confirmação de cadastro',
      template_name: 'registration_confirmation'
    )
  end

  def password_recovery(link, user, human_time, browser)
    @reset_link = link
    @human_time = human_time
    @browser = browser
    mail(
      from: Rails.configuration.email_settings['email'],
      to: user.email,
      subject: '[PDMSys] Esqueceu sua senha?',
      template_name: 'password_recovery'
    )
  end
end
