# frozen_string_literal: true
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  scope :by_name, ->(name) { where('name LIKE ?', "%#{name}%") }
  scope :by_current_user, -> { where(user_id: CurrentScope.user['id']) }

  private

  def current_scope?
    ApplicationRecord.const_defined?('CurrentScope')
  end

  def set_default_user
    self.user_id = if current_scope?
      CurrentScope.user['id'] || User.first.id
    else
      User.first.id
    end
  end
end
