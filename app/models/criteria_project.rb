# frozen_string_literal: true
class CriteriaProject < ApplicationRecord
  belongs_to :criterium, inverse_of: :criteria_projects
  belongs_to :project, inverse_of: :criteria_projects

  validates :criterium, presence: true
  validates :project, presence: true
end
