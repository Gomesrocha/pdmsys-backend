# frozen_string_literal: true
class CriteriaProjectGroup < ApplicationRecord
  belongs_to :criterium, inverse_of: :criteria_project_groups
  belongs_to :project_group, inverse_of: :criteria_project_groups

  validates :weight,
            presence: true,
            numericality: {
              greater_than_or_equal_to: 0,
              less_than_or_equal_to: 10,
              only_integer: true
            }
  validates :criterium, presence: true
  validates :project_group, presence: true
end
