# frozen_string_literal: true
class Criterium < ApplicationRecord
  scope :by_availability, -> { by_current_user.or(where(default_item: true)) }

  enum kind: { Benefício: 1, Custo: 2 }

  belongs_to :user, inverse_of: :criteria

  has_many  :criteria_projects,
            dependent: :restrict_with_error,
            inverse_of: :criterium
  has_many  :criteria_project_groups,
            dependent: :restrict_with_error,
            inverse_of: :criterium

  validates :name, presence: true
  validates :name,
            length: { minimum: 3, maximum: 255 },
            uniqueness: { case_sensitive: false, scope: :user_id },
            unless: 'name.blank?'

  validates :description,
            length: { minimum: 3 },
            allow_blank: true

  validates :kind, presence: true

  # validate :name_uniqueness

  before_validation :set_default_user

  # private

  # def name_uniqueness
  #   if name.present? && current_scope?
  #     exists = Criterium.by_availability.exists?(name: name)
  #     errors.add(:name, :taken) if exists
  #   else
  #     false
  #   end
  # end
end
