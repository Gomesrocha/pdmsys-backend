# frozen_string_literal: true
class Project < ApplicationRecord
  belongs_to :project_group, inverse_of: :projects

  has_many :criteria_projects, dependent: :delete_all, inverse_of: :project
  has_many :project_dependents, dependent: :delete_all, inverse_of: :project
  has_many  :project_dependents_dep,
            dependent: :delete_all,
            inverse_of: :project_dep,
            foreign_key: :project_dependent_id,
            class_name: 'ProjectDependent'

  accepts_nested_attributes_for :criteria_projects,
                                :project_dependents,
                                allow_destroy: true,
                                reject_if: proc { |att|
                                  att['id'].blank? && att['criterium_id'].blank?
                                }

  validates :name, presence: true
  validates :name,
            length: { minimum: 3, maximum: 255 },
            unless: 'name.blank?'

  validates :description,
            length: { minimum: 3 },
            allow_blank: true

  validates :project_group, presence: true
end
