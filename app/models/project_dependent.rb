# frozen_string_literal: true
class ProjectDependent < ApplicationRecord
  attr_accessor :_new

  belongs_to :project, inverse_of: :project_dependents
  belongs_to  :project_dep,
              inverse_of: :project_dependents_dep,
              class_name: 'Project'

  validates :project, presence: true
end
