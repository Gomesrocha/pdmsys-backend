# frozen_string_literal: true
class ProjectGroup < ApplicationRecord
  scope :by_name, ->(name) { where('project_groups.name LIKE ?', "%#{name}%") }

  belongs_to :user, inverse_of: :project_groups

  has_many :criteria, through: :criteria_project_groups
  has_many  :criteria_project_groups,
            dependent: :delete_all,
            inverse_of: :project_group
  has_many :criteria_projects, through: :projects
  has_many :projects, dependent: :destroy, inverse_of: :project_group
  has_many :project_dependents, through: :projects

  accepts_nested_attributes_for :criteria_project_groups,
                                :projects,
                                allow_destroy: true

  validates :name, presence: true
  validates :name,
            length: { minimum: 3, maximum: 255 },
            uniqueness: { case_sensitive: false, scope: :user_id },
            unless: 'name.blank?'

  validates :description,
            length: { minimum: 3 },
            allow_blank: true

  validate :validate_projects

  before_validation :set_default_user

  private

  def validate_projects
    if projects.empty?
      errors.add(:base, 'Cada grupo de projeto deve conter ao menos 1 projeto')
    else
      validate_uniqueness_of_in_memory(projects,
                                       [:name],
                                       'Os nomes dos projetos devem ser únicos')
    end
  end
end
