# frozen_string_literal: true
require 'tsort'

class Sorter
  include TSort

  def initialize(projects)
    @projects = projects
  end

  def tsort_each_node(&block)
    @projects.each_key(&block)
  end

  def tsort_each_child(node, &block)
    @projects[node].each(&block)
  end
end
