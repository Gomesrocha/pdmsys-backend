# frozen_string_literal: true
class Timeline < ApplicationRecord
  belongs_to :timeline_category, inverse_of: :timelines
  belongs_to :user, inverse_of: :timelines

  def self.by_date(date)
    where(%{
            DAY(created_at) = ? AND
            MONTH(created_at) = ? AND
            YEAR(created_at) = ?
          }.squish,
          date.day, date.month, date.year)
  end
end
