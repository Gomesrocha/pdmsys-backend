# frozen_string_literal: true
class TimelineCategory < ApplicationRecord
  has_many  :timelines, dependent: :restrict_with_error,
            inverse_of: :timeline_category
end
