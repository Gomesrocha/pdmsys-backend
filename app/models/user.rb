# frozen_string_literal: true
class User < ActiveRecord::Base
  scope :by_email, ->(email) { where('email LIKE ?', "%#{email}%") }

  attr_accessor :encoded_avatar

  has_secure_password

  belongs_to :user_type, inverse_of: :users

  has_many :criteria, inverse_of: :user
  has_many :project_groups, inverse_of: :user
  has_many :timelines, inverse_of: :user

  has_attached_file :avatar,
                    default_url: '/images/default/no-photo.png',
                    styles: {
                      large: '600x600>',
                      medium: '300x300>',
                      thumb: '100x100>'
                    },
                    url: '/images/:class/:id/:style/:filename',
                    use_timestamp: false

  validates_attachment :avatar,
                       content_type: { content_type: %r{image/} }

  validates :email, presence: true
  validates :email,
            uniqueness: { case_sensitive: false },
            length: { maximum: 255 },
            format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i },
            unless: 'email.blank?'

  validates :name,
            length: { minimum: 3, maximum: 255 },
            unless: 'name.blank?'

  validates :nickname,
            length: { minimum: 3, maximum: 255 },
            unless: 'nickname.blank?'

  validates :password,
            confirmation: true,
            length: { minimum: 8, maximum: 255 },
            unless: 'password.blank?'

  before_validation :parse_image, on: :update
  before_validation :set_default_user_type, on: :create
  before_destroy :delete_associations, prepend: true

  def avatar_url
    avatar.url(:medium)
  end

  private

  def delete_associations
    ProjectGroup.where(user_id: id).destroy_all
    Criterium.where(user_id: id).delete_all
  end

  def parse_image
    file = Paperclip.io_adapters.for(encoded_avatar)
    file.original_filename = file.content_type.sub('image/', 'profile.')
    self.avatar = file
  end

  def set_default_user_type
    self.user_type ||= UserType.find_by(name: 'Cliente')
  end
end
