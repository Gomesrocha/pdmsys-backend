# frozen_string_literal: true
class UserType < ApplicationRecord
  has_many :users, dependent: :restrict_with_error, inverse_of: :user_type

  validates :access_level,
            presence: true,
            numericality: {
              greater_than_or_equal_to: 1,
              less_than_or_equal_to: 5,
              only_integer: true
            }

  validates :description,
            length: { minimum: 3 },
            allow_blank: true

  validates :name, presence: true
  validates :name,
            length: { minimum: 3, maximum: 255 },
            uniqueness: true,
            unless: 'name.blank?'
end
