# frozen_string_literal: true
module Api
  module V1
    class CriteriaProjectGroupSerializer < ActiveModel::Serializer
      attributes :id, :weight, :criterium_id, :project_group_id
    end
  end
end
