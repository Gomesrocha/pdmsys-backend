# frozen_string_literal: true
module Api
  module V1
    class CriteriaProjectSerializer < ActiveModel::Serializer
      attributes :id, :attr, :criterium_id, :project_id
    end
  end
end
