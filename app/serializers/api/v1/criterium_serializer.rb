# frozen_string_literal: true
module Api
  module V1
    class CriteriumSerializer < ActiveModel::Serializer
      attributes :id, :name, :description, :kind, :default_item
    end
  end
end
