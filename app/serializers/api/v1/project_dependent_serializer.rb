# frozen_string_literal: true
module Api
  module V1
    class ProjectDependentSerializer < ActiveModel::Serializer
      attributes :project_id, :project_dependent_id
    end
  end
end
