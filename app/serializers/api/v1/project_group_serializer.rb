# frozen_string_literal: true
module Api
  module V1
    class ProjectGroupSerializer < ActiveModel::Serializer
      attributes :id, :name, :description

      has_many  :criteria,
                through: :criteria_project_groups,
                unless: :index?
      has_many  :criteria_project_groups,
                inverse_of: :project_group,
                unless: :index?
      has_many  :criteria_projects,
                through: :projects,
                unless: :index?
      has_many  :projects,
                inverse_of: :project_group,
                unless: :index?
      has_many  :project_dependents,
                through: :projects,
                unless: :index?

      def index?
        action_name == 'index'
      end
    end
  end
end
