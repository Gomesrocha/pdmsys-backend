# frozen_string_literal: true
module Api
  module V1
    class ProjectSerializer < ActiveModel::Serializer
      attributes :id, :name, :description, :project_group_id
    end
  end
end
