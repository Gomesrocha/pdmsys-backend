# frozen_string_literal: true
module Api
  module V1
    class TimelineSerializer < ActiveModel::Serializer
      attributes  :id, :action, :browser_name, :browser_version, :ip, :message,
                  :created_at
      belongs_to :user
      belongs_to :timeline_category
    end
  end
end
