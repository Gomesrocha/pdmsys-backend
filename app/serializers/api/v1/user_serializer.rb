# frozen_string_literal: true
module Api
  module V1
    class UserSerializer < ActiveModel::Serializer
      attributes :id, :name, :email, :nickname
      attribute :avatar_url, if: :avatar?

      def avatar?
        object.avatar_file_name
      end
    end
  end
end
