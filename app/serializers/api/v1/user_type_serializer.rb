# frozen_string_literal: true
module Api
  module V1
    class UserTypeSerializer < ActiveModel::Serializer
      attributes :id, :access_level, :description, :name
    end
  end
end
