# frozen_string_literal: true
ActiveRecordQueryTrace.enabled = true
ActiveRecordQueryTrace.colorize = 'light purple'
