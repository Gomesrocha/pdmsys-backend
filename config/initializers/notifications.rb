# frozen_string_literal: true
ActiveSupport::Notifications
  .subscribe('rack.attack') do |_name, _start, _finish, _id, payload|
  next unless payload.env['rack.attack.match_type'] == :blocklist

  Rails.logger.info "IP #{payload.ip} blocked for 1 hour from "\
                    "#{Time.now.strftime('%d/%m/%Y %H:%M:%S')}"
end
