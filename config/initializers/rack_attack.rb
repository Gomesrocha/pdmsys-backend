class Rack::Attack
  Rack::Attack.cache.store = ActiveSupport::Cache::MemoryStore.new

  Rack::Attack.safelist('allow from localhost') do |req|
    req.ip == '127.0.0.1' || req.ip == '::1'
  end

  Rack::Attack.blocklist('allow2ban') do |req|
    Rack::Attack::Allow2Ban.filter(req.ip, maxretry: 20, findtime: 1.minute, bantime: 1.hour) do
      req.ip
    end
  end

  Rack::Attack.blocklisted_response = lambda do |env|
    [503, {}, [{ _body: 'Server Error' }.to_json]]
  end
end
