# frozen_string_literal: true
module ActiveRecord
  class Base
    def validate_uniqueness_of_in_memory(collection, attrs, message)
      hashes = collection.each.with_object({}) do |record, hash|
        key = attrs.map { |a| record.send(a).to_s }.join
        key = record.object_id if key.blank? || record.marked_for_destruction?
        hash[key] = record unless hash[key]
      end
      errors.add(:base, message) if collection.length > hashes.length
    end
  end
end
