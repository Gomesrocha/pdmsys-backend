Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  scope module: :api do
    namespace :v1 do
      resources :criteria
      resources :criterium_types, only: :index
      resources :project_groups do
        get ':id/edit', on: :collection, action: :edit
      end
      resources :timelines, only: :index
      resources :users do
        collection do
          post :confirm_email
          post :recover_password
          post :resend_email
          post :reset_password
          post :sign_in
          post :sign_out
        end
      end
      resources :user_types
    end
  end
end
