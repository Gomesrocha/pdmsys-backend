class CreateUserTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :user_types do |t|
      t.integer :access_level, null: false
      t.text :description
      t.string :name, null: false

      t.timestamps
    end
  end
end
