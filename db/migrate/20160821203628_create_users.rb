class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.attachment :avatar
      t.string :email, null: false
      t.string :name
      t.string :nickname
      t.string :password_digest, null: false
      t.boolean :email_confirmed, default: false, null: false
      t.text :email_confirmation_token
      t.text :reset_password_token
      t.references :user_type, foreign_key: true, null: false
      t.index :email, unique: true

      t.timestamps
    end
  end
end
