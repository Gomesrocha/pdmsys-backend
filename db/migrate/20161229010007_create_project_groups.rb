class CreateProjectGroups < ActiveRecord::Migration[5.0]
  def change
    create_table :project_groups do |t|
      t.text :description
      t.string :name, null: false
      t.references :user, foreign_key: true, null: false
      t.index [:name, :user_id], unique: true

      t.timestamps
    end
  end
end
