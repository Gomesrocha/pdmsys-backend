class CreateProjects < ActiveRecord::Migration[5.0]
  def change
    create_table :projects do |t|
      t.text :description
      t.string :name, null: false
      t.references :project_group, foreign_key: true, null: false
      t.index [:name, :project_group_id], unique: true

      t.timestamps
    end
  end
end
