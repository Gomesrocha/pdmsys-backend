class CreateProjectDependents < ActiveRecord::Migration[5.0]
  def change
    create_table :project_dependents, id: false do |t|
      t.references :project, foreign_key: { to_table: :projects }, null: false
      t.references :project_dependent, foreign_key: { to_table: :projects }, null: false
      t.index [:project_id, :project_dependent_id], unique: true
    end
  end
end
