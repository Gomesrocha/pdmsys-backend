class CreateCriteria < ActiveRecord::Migration[5.0]
  def change
    create_table :criteria do |t|
      t.boolean :default_item, default: false, null: false
      t.text :description
      t.integer :kind, default: 1, null: false
      t.string :name, null: false
      t.references :user, foreign_key: true, null: false
      t.index [:name, :user_id], unique: true

      t.timestamps
    end
  end
end
