class CreateCriteriaProjectGroups < ActiveRecord::Migration[5.0]
  def change
    create_table :criteria_project_groups do |t|
      t.integer :weight, null: false
      t.references :criterium, foreign_key: true, null: false
      t.references :project_group, foreign_key: true, null: false
      t.index [:criterium_id, :project_group_id], name: :index_criteria_project_groups_criterium_id_project_group_id, unique: true

      t.timestamps
    end
  end
end
