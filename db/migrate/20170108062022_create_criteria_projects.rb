class CreateCriteriaProjects < ActiveRecord::Migration[5.0]
  def change
    create_table :criteria_projects do |t|
      t.string :attr, null: false
      t.references :criterium, foreign_key: true, null: false
      t.references :project, foreign_key: true, null: false
      t.index [:criterium_id, :project_id], unique: true

      t.timestamps
    end
  end
end
