class CreateTimelines < ActiveRecord::Migration[5.0]
  def change
    create_table :timelines do |t|
      t.string :action, null: false
      t.string :browser_name, null: false
      t.string :browser_version, null: false
      t.string :ip, null: false
      t.string :message
      t.references :timeline_category, foreign_key: true, null: false
      t.references :user, foreign_key: true, null: false

      t.timestamps
    end
  end
end
