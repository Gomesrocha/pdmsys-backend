# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170403010709) do

  create_table "criteria", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.boolean  "default_item",               default: false, null: false
    t.text     "description",  limit: 65535
    t.integer  "kind",                       default: 1,     null: false
    t.string   "name",                                       null: false
    t.integer  "user_id",                                    null: false
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.index ["name", "user_id"], name: "index_criteria_on_name_and_user_id", unique: true, using: :btree
    t.index ["user_id"], name: "index_criteria_on_user_id", using: :btree
  end

  create_table "criteria_project_groups", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "weight",           null: false
    t.integer  "criterium_id",     null: false
    t.integer  "project_group_id", null: false
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["criterium_id", "project_group_id"], name: "index_criteria_project_groups_criterium_id_project_group_id", unique: true, using: :btree
    t.index ["criterium_id"], name: "index_criteria_project_groups_on_criterium_id", using: :btree
    t.index ["project_group_id"], name: "index_criteria_project_groups_on_project_group_id", using: :btree
  end

  create_table "criteria_projects", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "attr",         null: false
    t.integer  "criterium_id", null: false
    t.integer  "project_id",   null: false
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["criterium_id", "project_id"], name: "index_criteria_projects_on_criterium_id_and_project_id", unique: true, using: :btree
    t.index ["criterium_id"], name: "index_criteria_projects_on_criterium_id", using: :btree
    t.index ["project_id"], name: "index_criteria_projects_on_project_id", using: :btree
  end

  create_table "project_dependents", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "project_id",           null: false
    t.integer "project_dependent_id", null: false
    t.index ["project_dependent_id"], name: "index_project_dependents_on_project_dependent_id", using: :btree
    t.index ["project_id", "project_dependent_id"], name: "index_project_dependents_on_project_id_and_project_dependent_id", unique: true, using: :btree
    t.index ["project_id"], name: "index_project_dependents_on_project_id", using: :btree
  end

  create_table "project_groups", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.text     "description", limit: 65535
    t.string   "name",                      null: false
    t.integer  "user_id",                   null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["name", "user_id"], name: "index_project_groups_on_name_and_user_id", unique: true, using: :btree
    t.index ["user_id"], name: "index_project_groups_on_user_id", using: :btree
  end

  create_table "projects", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.text     "description",      limit: 65535
    t.string   "name",                           null: false
    t.integer  "project_group_id",               null: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.index ["name", "project_group_id"], name: "index_projects_on_name_and_project_group_id", unique: true, using: :btree
    t.index ["project_group_id"], name: "index_projects_on_project_group_id", using: :btree
  end

  create_table "timeline_categories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name",       null: false
    t.string   "icon",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "timelines", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "action",               null: false
    t.string   "browser_name",         null: false
    t.string   "browser_version",      null: false
    t.string   "ip",                   null: false
    t.string   "message"
    t.integer  "timeline_category_id", null: false
    t.integer  "user_id",              null: false
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.index ["timeline_category_id"], name: "index_timelines_on_timeline_category_id", using: :btree
    t.index ["user_id"], name: "index_timelines_on_user_id", using: :btree
  end

  create_table "user_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "access_level",               null: false
    t.text     "description",  limit: 65535
    t.string   "name",                       null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "email",                                                  null: false
    t.string   "name"
    t.string   "nickname"
    t.string   "password_digest",                                        null: false
    t.boolean  "email_confirmed",                        default: false, null: false
    t.text     "email_confirmation_token", limit: 65535
    t.text     "reset_password_token",     limit: 65535
    t.integer  "user_type_id",                                           null: false
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["user_type_id"], name: "index_users_on_user_type_id", using: :btree
  end

  add_foreign_key "criteria", "users"
  add_foreign_key "criteria_project_groups", "criteria"
  add_foreign_key "criteria_project_groups", "project_groups"
  add_foreign_key "criteria_projects", "criteria"
  add_foreign_key "criteria_projects", "projects"
  add_foreign_key "project_dependents", "projects"
  add_foreign_key "project_dependents", "projects", column: "project_dependent_id"
  add_foreign_key "project_groups", "users"
  add_foreign_key "projects", "project_groups"
  add_foreign_key "timelines", "timeline_categories"
  add_foreign_key "timelines", "users"
  add_foreign_key "users", "user_types"
end
