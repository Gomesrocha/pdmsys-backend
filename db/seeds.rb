# User types

user_types = [
  {
    access_level: 5,
    name: 'SuperAdmin'
  },
  {
    access_level: 4,
    name: 'Admin'
  },
  {
    access_level: 1,
    name: 'Cliente'
  }
]

UserType.create(user_types)

p "#{user_types.length} regra(s) criada(s)!"

# Users

users = [
  {
    email: 'rafaelsantana3095@gmail.com',
    password: 'rafael123',
    email_confirmed: true,
    user_type_id: UserType.find_by(name: 'SuperAdmin').id
  }
]

User.create(users)

p "#{users.length} usuário(s) criado(s)!"

# Criteria

benefit_kind = 1
cost_kind = 2

criteria = [
  {
    name: 'Custo',
    default_item: true,
    kind: cost_kind
  },
  {
    name: 'ROI',
    default_item: true,
    kind: benefit_kind
  },
  {
    name: 'Exigência de Norma/Lei',
    default_item: true,
    kind: benefit_kind
  },
  {
    name: 'Evita Colapso na infraestrutura',
    default_item: true,
    kind: benefit_kind
  },
  {
    name: 'Alinhamento estratégico',
    default_item: true,
    kind: benefit_kind
  },
  {
    name: 'Principais stakeholders beneficiados',
    default_item: true,
    kind: benefit_kind
  },
  {
    name: 'Riscos envolvidos',
    default_item: true,
    kind: cost_kind
  },
  {
    name: 'Probabilidade de entrega',
    default_item: true,
    kind: benefit_kind
  },
  {
    name: 'Esforço',
    default_item: true,
    kind: cost_kind
  },
  {
    name: 'Otimização de recursos',
    default_item: true,
    kind: benefit_kind
  }
]

Criterium.create(criteria)

p "#{criteria.length} critério(s) criado(s)!"


timeline_categories = [
  { name: 'Critério', icon: 'fa fa-pencil bg-purple' },
  { name: 'Grupo de projeto', icon: 'fa fa-briefcase bg-olive' },
  { name: 'Tipo de usuário', icon: 'fa fa-flag bg-blue' },
  { name: 'Usuário', icon: 'fa fa-user bg-red' },
  { name: 'SignIn', icon: 'fa fa-sign-in bg-navy' },
  { name: 'SignOut', icon: 'fa fa-sign-out bg-teal' }
]

TimelineCategory.create(timeline_categories)

p "#{timeline_categories.length} categorias(s) de linha do tempo criada(s)!"
