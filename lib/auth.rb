# frozen_string_literal: true
require 'jwt'

class Auth
  ALGORITHM = 'HS256'
  AUTH_SECRET = Rails.application.secrets.secret_key_base

  def self.decode(token)
    JWT.decode(token, AUTH_SECRET, true, algorithm: ALGORITHM).first
  end

  def self.encode(payload)
    JWT.encode(payload, AUTH_SECRET, ALGORITHM)
  end

  def self.generate_payload(user, exp = 4.hours.from_now.to_i)
    {
      aud: 'client',
      exp: exp,
      iss: 'issuer_name',
      user: user
    }
  end
end
