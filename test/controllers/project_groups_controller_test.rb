require 'test_helper'

class ProjectGroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @project_group = project_groups(:one)
  end

  test "should get index" do
    get project_groups_url, as: :json
    assert_response :success
  end

  test "should create project_group" do
    assert_difference('ProjectGroup.count') do
      post project_groups_url, params: { project_group: { description: @project_group.description, name: @project_group.name, user_id: @project_group.user_id } }, as: :json
    end

    assert_response 201
  end

  test "should show project_group" do
    get project_group_url(@project_group), as: :json
    assert_response :success
  end

  test "should update project_group" do
    patch project_group_url(@project_group), params: { project_group: { description: @project_group.description, name: @project_group.name, user_id: @project_group.user_id } }, as: :json
    assert_response 200
  end

  test "should destroy project_group" do
    assert_difference('ProjectGroup.count', -1) do
      delete project_group_url(@project_group), as: :json
    end

    assert_response 204
  end
end
